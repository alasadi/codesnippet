<?php

use Illuminate\Database\Seeder;
use App\Models\Publication;


class PublicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Publication::insert([
            ['publications' => 'Public'],
            ['publications' => 'Private']
        ]);

    }
}
