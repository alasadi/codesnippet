<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	User::insert([
            [
                'email' => 'test@test.com',
                'password' => bcrypt('secret'),
                'remember_token' => str_random(10),
                'registration_date' => date('Y-m-d h:m:s'),
                'api_token' => '$2y$10$159rLQKktQYReMrBUewHQ.O/cTfa0VmA4Wdc/aYQo8sgNW5ycAJ2m',
                'type' => 'admin'
            ],
            // [
            //     'email' => 'test2@test.com',
            //     'password' => bcrypt('secret'),
            //     'remember_token' => str_random(10),
            //     'registration_date' => date('Y-m-d h:m:s'),
            //     'api_token' => '$2y$10$159rLQKktQYReMrBUewHQ.O/cTfa0VmA4Wdc/aYQo8sgNW5yc351',
            //     'type' => 'admin'
                
            // ]
        ]);   
    }
}
