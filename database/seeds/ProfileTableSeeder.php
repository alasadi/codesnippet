<?php

use Illuminate\Database\Seeder;
use App\Models\UserProfile;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserProfile::insert(
            [
                [
                'firstname' => 'Ali',
                'lastname' => 'Alasadi',
                'email' => 'test@test.com',
                'website' => 'http://test.com',
                'qoute' => 'nothing is real',
                'profile_pic' => 'https://picsum.photos/id/651/200/200',
                'status_id' => 0,
                'facebook' => 'facebook.com/alialasadi',
                'twitter' => 'twitter.com/alialasadi',
                'linkedin' => 'linkedin.com/alialasadi',
                'github' => 'github.com/alialasadi',
                'about_me' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis consequuntur enim quos, obcaecati vel sapiente eos deserunt voluptas laudantium architecto ducimus nulla tempora nemo earum dignissimos repellat maxime necessitatibus hic. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam, ea quos. Suscipit unde consectetur perferendis optio velit ad laborum, nihil sed incidunt magnam ipsum, recusandae harum. Architecto rerum minima quisquam! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Unde autem neque accusamus dicta maxime nostrum, excepturi eius ipsa mollitia consequatur, recusandae veritatis corporis sunt ex placeat. Nulla ex modi eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores tempore similique eum ex repellendus, culpa accusantium aspernatur impedit officia. Natus ea beatae distinctio provident a sed optio ipsa porro praesentium. Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam optio obcaecati, magnam ratione modi ad libero explicabo non minus? Eligendi harum ut nulla debitis unde provident, voluptates reprehenderit repellat mollitia?',
               
                'work_at' => 'CodeSnippet GMBH',
                'position' => 'CEO',
                'user_id' => 1
            ]
        ]);
    }
}