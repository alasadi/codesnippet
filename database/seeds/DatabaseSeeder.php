<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PublicationsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        factory('App\Models\Userprofile', 10)->create();
        factory('App\Models\User', 10)->create();
    }
}
