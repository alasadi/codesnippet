<?php

use Illuminate\Database\Seeder;
use App\Models\Languages;


class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Languages::insert([
            ['language_name' => 'JavaScript'],
            ['language_name' => 'Java'],
            ['language_name' => 'Python'],
            ['language_name' => 'Ruby'],
            ['language_name' => 'PHP'],
            ['language_name' => 'CSS'],
            ['language_name' => 'C++'],
            ['language_name' => 'C#'],
            ['language_name' => 'C'],
            ['language_name' => 'Visual Basic .NET'],
            ['language_name' => 'Perl'],
            ['language_name' => 'Swift'],
            ['language_name' => 'Objective-C']
        ]);
    }
}
