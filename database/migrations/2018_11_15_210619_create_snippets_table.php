<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snippets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('snippet_name');
            $table->text('short_snippet')->nullable();
            $table->text('snippet');
            $table->string('created_at')->timestamps();
            $table->string('updated_at')->timestamps();
            $table->string('tags')->nullable();
            $table->integer('user_id');
            $table->boolean('snippet_status')->default(0);
            $table->boolean('language_id')->default(1);
            $table->boolean('plugin_upload')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snippets');
    }
}
