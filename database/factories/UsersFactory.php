<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
        'api_token' => '$2y$10$159rLQKktQYReMrBUewHQ.O/cTfa0VmA4Wdc/aYQo8sgNW5yc351',
        'registration_date' => '2019-06-10 03:06:11',
        'type' => 'default'
    ];
});
