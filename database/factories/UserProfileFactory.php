<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Userprofile::class, function (Faker $faker) {
    return [
            'firstname' => $faker->firstname,
            'lastname' => $faker->lastname,
            'email' => $faker->unique()->email,
            'password' => bcrypt('secret'),
            'website' => $faker->unique()->domainName,
            'qoute' => 'Sit vitae voluptas sint non voluptates.',
            'profile_pic' => 'https://picsum.photos/200/200?random',
            'status_id' => 0,
            'facebook' => $faker->unique()->domainName,
            'twitter' => $faker->unique()->domainName,
            'linkedin' => $faker->unique()->domainName,
            'github' => $faker->unique()->domainName,
            'about_me' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis consequuntur enim quos, obcaecati vel sapiente eos deserunt voluptas laudantium architecto ducimus nulla tempora nemo earum dignissimos repellat maxime necessitatibus hic. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam, ea quos. Suscipit unde consectetur perferendis optio velit ad laborum, nihil sed incidunt magnam ipsum, recusandae harum. Architecto rerum minima quisquam! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Unde autem neque accusamus dicta maxime nostrum, excepturi eius ipsa mollitia consequatur, recusandae veritatis corporis sunt ex placeat. Nulla ex modi eum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores tempore similique eum ex repellendus, culpa accusantium aspernatur impedit officia. Natus ea beatae distinctio provident a sed optio ipsa porro praesentium. Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam optio obcaecati, magnam ratione modi ad libero explicabo non minus? Eligendi harum ut nulla debitis unde provident, voluptates reprehenderit repellat mollitia?',
            'bio' => 'Code | Developer | JavaScript <br> <br> <br>

            Lorem ipsum dolor sit amet consectetur adipisicing elit. At, optio? Autem nulla quidem sequi sit, eaque, minima assumenda qui inventore doloribus laudantium nobis tenetur omnis non distinctio, provident debitis vero!',
            'work_at' => 'CodeSnippet',
            'position' => 'CEO'
    ];
});
