<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Mail\Welcome;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(32));

        if (User::where('user_token', $token)) {
            $token = bin2hex(openssl_random_pseudo_bytes(32));
        }
        $userold = User::where('email', $request['email'])->get();
        if ($userold->count() > 0) {
            return response()->redirectToRoute('login')->with('info', "This email is already registered.");
        }


        $user = new User;
        $user->email = $request['email'];
        $user->password = '';
        $user->registration_date = date('Y-m-d h:m:s');
        $user->api_token = $token; 
        $user->active = 1;
        $user->type = User::DEFAULT_TYPE;
        
        $user->save();

        $data = array('name' => 'codesnippet', 'token' => $token);

        Mail::send('mailtemplate.registrationMail', $data, function($message) use($request)
        {
             $message->to($request['email'])
             ->subject('Hi there!  Laravel sent me!');
        });

        return response()->redirectToRoute('login')->with('success', "You will get an email to complete your registration");
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
