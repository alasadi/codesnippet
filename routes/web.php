<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/');
});

Auth::routes();

Route::get('/home', ['uses' => 'HomeController@index', 'as' => 'index']);
Route::get('/', ['uses' => 'HomeController@index', 'as' => 'index']);

Route::get('logout', 'Auth\LoginController@logout');


Route::get('/registration', ['uses'=>'Auth\RegisterController@showRegister', 'as'=>'auth.registration']);
Route::post('/registration', ['uses'=>'Auth\RegisterController@registerWithPassword', 'as'=>'auth.register']);

Route::get('/result', ['uses' => 'Snippets\SnippetsController@search', 'as' => 'sites.results']);

Route::get('/result?searchTag={tag}', ['uses' => 'Snippets\SnippetsController@search', 'as' => 'searchTag']);

//snippets/profile 
Route::get('/user/{id}', ['uses'=> 'ProfileController@Profile', 'as' =>'sites.profile']);


Route::middleware('auth')->group(function() {
	//Snippets get
	Route::get('/meine_snippets', ['uses'=> 'Snippets\SnippetsController@meineSnippets', 'as'=> 'sites.meineSnippets']);
	Route::get('/snippet/{id}/edit', ['uses'=> 'Snippets\SnippetsController@editSnippet', 'as' =>'sites.editSnippet']);
	Route::get('/snippet/create', ['uses'=> 'Snippets\SnippetsController@createSnippet', 'as' => 'sites.createSnippet']);
	
	//Snippets post
	Route::post('/snippet/create', ['uses'=> 'Snippets\SnippetsController@storeSnippet', 'as'=>'sites.createSnippet']);
	Route::post('/snippet/{id}/edit', ['uses'=> 'Snippets\SnippetsController@updateSnippet', 'as'=>'sites.updateSnippet']);
	Route::post('/meine_snippets/{id}', ['uses'=> 'Snippets\SnippetsController@deleteSnippet', 'as'=>'sites.deleteSnippet']);
	
	//Profile

	Route::post('/user/{id}', ['uses'=> 'ProfileController@ProfileEdit', 'as' =>'profilepost']);


	//Follow
	Route::post('user/{id}/follow', ['uses' => 'FollowerController@followUser', 'as' => 'user.follow']);
	Route::post('user/{id}/unfollow', ['uses' => 'FollowerController@unFollowUser', 'as' => 'user.unfollow']);


	Route::get('/user/{id}/followers', ['uses'=> 'FollowerController@showFollowers', 'as'=>'sites.followers']);
	Route::get('/user/{id}/followering', ['uses'=> 'FollowerController@showFollowing', 'as'=>'sites.showFollowing']);

});



Route::get('/howto', ['uses'=> 'HomeController@howto', 'as'=>'sites.howto']);

Route::get('/snippets', ['uses'=> 'Snippets\SnippetsController@Snippets', 'as'=>'sites.snippets']);
Route::get('/snippet/{id}', ['uses'=> 'Snippets\SnippetsController@showSnippet', 'as'=>'sites.showSnippet']);

Route::get('/user/{id}/edit', ['uses'=> 'ProfileController@ProfileEditGet', 'as' => 'sites.editprofile']);

//CMS
Route::get('cms/login', 'CMS\CmsController@login')->name('cms.login');
Route::post('cms/login', 'CMS\CmsController@loginPost' )->name('cms.loginPost');
Route::get('cms/', 'CMS\CmsController@logout')->name('cms.logout');

Route::middleware('is_admin')->group(function(){
	Route::get('cms/users', 'CMS\CmsController@users')->name('cms.users');
	Route::get('cms/userdetail/{id}', 'CMS\CmsController@usersDetail')->name('cms.userdetail');

	Route::post('cms/users/{id}', 'CMS\CmsController@deleteUser')->name('cms.deleteUser');

	Route::post('cms/userdetail/{id}', 'CMS\CmsController@userPost')->name('cms.usersPost');
	Route::get('cms/createuser', 'CMS\CmsController@createuser')->name('cms.createUser');
	Route::post('cms/createuser', 'CMS\CmsController@createUserPost')->name('cms.createUserPost');

	Route::get('cms/snippets', 'CMS\CmsController@snippets')->name('cms.snippets');
	Route::get('cms/snippetdetail/{id}', 'CMS\CmsController@snippetDetail')->name('cms.snippetdetail');

	Route::post('cms/snippetdetail/{id}/edit', 'CMS\CmsController@storeSnippet')->name('cms.snippetsPost');
	Route::post('cms/snippets/{id}', 'CMS\CmsController@deleteSnippet')->name('cms.deleteSnippet');

	Route::get('cms/charts', 'CMS\CmsController@charts')->name('cms.charts');
});


