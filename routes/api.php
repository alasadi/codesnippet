<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/snippets', 'api\SnippetsControlller@index');
Route::get('/mysnippets/{user_id}', 'api\SnippetsControlller@mySnippets');

Route::get('/followers', 'api\followController@followers');
Route::get('/following', 'api\followController@following');
Route::get('/myfollowers', 'api\followController@myFollowers');

Route::post('/follow/{id}/{authid}', 'api\followController@followPost');

Route::post('/unfollow/{id}/{authid}', 'api\followController@unfollowPost');

Route::get('/viewcount/{id}', 'api\SnippetsControlller@viewCount');
Route::get('/mostvisited/', 'api\ChartsController@mostvisited');
Route::get('/uploadedfrom/', 'api\ChartsController@uploadedFrom');
Route::get('/loggedinusers/', 'api\ChartsController@loggedinUsers');
Route::get('/mostusedlanguages/', 'api\ChartsController@mostusedLanguages');


Route::post('/upvote/{id}/{authid}', 'api\RatingController@upvote');
Route::post('/downvote/{id}/{authid}', 'api\RatingController@downvote');
Route::post('/deletevote/{id}/{authid}/{vote}', 'api\RatingController@destroyVote');

Route::post('/newsletter/{email}', 'api\Newsletter@Updateuser');


Route::middleware('auth:api')->group(function(){
	Route::post('/', 'api\SnippetsControlller@store');
});




