<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userprofile extends Model
{
	public $timestamps = false;
	protected $fillable = ["firstname", "lastname", "email", "password","website", "qoute", "profil_pic", "facebook", "twitter", "linkedin", "about_me", "position", ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    	
    }
    public function followers()
    {
        return $this->belongsToMany(Userprofile::class, 'followers', 'follower_id', 'user_id')->withTimestamps();
    }
    public function followings()
    {
        return $this->belongsToMany(Userprofile::class, 'followers', 'user_id', 'follower_id')->withTimestamps();
    }
    public function snippets()
    {
        return $this->hasMany('App\Models\Snippets');
    }

}
