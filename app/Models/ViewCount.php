<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewCount extends Model
{
    public function snippets()
    {
    	return $this->belongsTo(Snippets::class);
    }
}
