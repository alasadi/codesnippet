<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Snippets extends Model
{
	protected $dates = ['created_at', 'updated_at'];

	protected $fillable = ['user_id', 'updated_at', 'created_at', 'snippet', 'snippet_name', 'language_id', 'snippet_short'];

    public function user()
    {
    	return $this->belongsTo(Userprofile::class);
    }

    public function lang()
    {
    	return $this->hasOne(Languages::class, 'id', 'language_id');
    }

    public function viewcount()
    {
        return $this->hasMany(ViewCount::class);
    }
}
