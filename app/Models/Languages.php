<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    public function snippets()
    {
    	return $this->hasMany(Snippets::class);
    }
}
