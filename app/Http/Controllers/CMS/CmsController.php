<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Userprofile;
use App\Models\Snippets;
use App\Models\Languages;
use App\Models\Publication;
use App\Models\Tags;

class CmsController extends Controller
{
    public function login()
    {
    	return view('cms.login');
    }
    public function loginPost(Request $request)
    {
    	 $this->validate($request, [

            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email','password']);
        $remember_me = $request->has('remember_me');

        if (auth()->attempt($credentials, $remember_me)) {

            return redirect()->intended(route('cms.charts'))->with('success', 'logged in!');

        } 

        return redirect()->route('cms.login')->with('error', 'invalid credentials!');
    }
    public function logout()
    {
        auth()->logout();

        return redirect()->route('cms.login')->with('info', 'Logged out');
    }
    public function users()
    {
        $users = User::all();

    	return view('cms.users', compact('users'));
    }
    public function usersDetail($id)
    {
        $user = User::findOrFail($id);
    	return view('cms.userdetail', compact('user'));
    }
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $username = $user->firstname;
        $user->profile->delete();
        $user->delete();

        return redirect()->route('cms.users')->with('info', "$username deleted!");

    }
    public function userPost(Request $request, $id)
    {

    	$user = User::findOrFail($id);


        $user->profile->fill($request->all());
        $user->type = $request['type'];

        $user->save();

        return redirect()->route('cms.userdetail', $user->id)->with('danger', 'edit user!');
        
    }    
    public function charts()
    {
        return view('cms.charts');
    }
    
    public function snippets()
    {
        $snippets = Snippets::all();
        return view('cms.snippets', compact('snippets'));
    }

    public function snippetDetail($id)
    {
    	$snippet = Snippets::findOrFail($id);
        $langs = Languages::all();
        $pubs = Publication::all();
        return view('cms.snippetDetail' , compact('snippet', 'langs', 'pubs'));
    }
    public function storeSnippet(Request $request)
    {
    	$snippet = new Snippets;
        $tags = new Tags;

        if ($request['uploadtxt']) 
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            ]);

            $destinationPath = "upload/";

            $file = $request['uploadtxt'];
            $file->move(base_path($destinationPath), $file->getClientOriginalName());
            $filePath = $destinationPath.$file->getClientOriginalName();
            $fileContent = file_get_contents('../'.$filePath);

            $snippet->snippet = $fileContent;
        }
        else
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            'snippet' => 'required',
            ]);

            $snippet->snippet = $request['snippet'];
        }
            $snippet->snippet_name = $request['snippetname'];
            $snippet->user_id = $request['user_id'];
            $snippet->language_id = $request['language'];
            $snippet->updated_at = Carbon::now();
            $snippet->short_snippet = substr($request['snippet'], 0, 254);
            $snippet->snippet_status = $request['is_public'];
            $snippet->tags = $request['tags'];
            $snippet->save();

            if ($request['uploadtxt']) 
            {
                unlink("../$filePath");
            }
        return redirect()->route('cms.snippets')->with('success', 'Snippet Created');  
    }
    public function deleteSnippet($id)
    {
        $snippet= Snippets::findOrFail($id);
        $snippet->viewcount()->delete();
        $snippet->delete();

        return redirect()->route('cms.snippets')->with('info', "Snippet deleted!");

    }
    public function createUser()
    {
       return view('cms.createUser');
    }
    public function createUserPost(Request $request)
    {
        $user = new User;

        $user->profile->fill($request->all());
        $user->type = $request['type'];

        $user->save();

        return redirect()->route('cms.userdetail', $user->id)->with('danger', 'edit user!');
    }

}
