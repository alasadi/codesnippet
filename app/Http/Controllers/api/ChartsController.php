<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Snippets;
use App\Models\ViewCount;
use App\Models\User;
use App\Models\Languages;

use App\Http\Resources\UserResource;
use App\Http\Resources\ChartResource;
use App\Http\Resources\LanguageResource;


class ChartsController extends Controller
{
    public function mostvisited()
    {
    	$counts = ViewCount::all()->groupBy('snippets_id')->take(10);
        $snippets = Snippets::all();
    	return ChartResource::collection($snippets);
    }
    public function uploadedFrom()
    {
    	$snippets = Snippets::all();
    	$uploadcount = [
    		'plugin' => 0,
    		'web' => 0
    	];
    	foreach ($snippets as $snippet) {

    		if ($snippet->plugin_upload == 1) {
    			$uploadcount['plugin']++;
    		}
    		else{
    			$uploadcount['web']++;
    		}
    	}

    	return $uploadcount;
    }
    public function loggedinUsers()
    {
    	$loggedinUsers = User::where('loggedin', '!=', '')->get()->take(10);

    	return UserResource::collection($loggedinUsers);
    }
    public function mostusedLanguages()
    {
    	$snippets = Snippets::all();
        $languages = Languages::all();
        $lang = [];
        foreach ($languages as $language => $key) {
        	$lang[$key->language_name] = 0;
        }
        foreach ($snippets as $snippet) {
        	if (in_array($snippet->lang->language_name, $lang)) {
        		$lang[$snippet->lang->language_name]++;
        	}
        }
    	return $lang;
    }
}
