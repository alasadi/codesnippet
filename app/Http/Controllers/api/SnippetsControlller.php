<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Http\Resources\SnippetResource;
use App\Http\Resources\MySnippetsResource;

use App\Models\Snippets;
use App\Models\User;
use App\Models\ViewCount;
use App\Models\Languages;


class SnippetsControlller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $snippets = Snippets::where('snippet_status', 1)->paginate(10);
        
        return SnippetResource::collection($snippets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function mySnippets(Request $request)
    {
        $snippets = Snippets::where('user_id', $request['user_id'])->get();
        return SnippetResource::collection($snippets);
    }

    public function store(Request $request)
    {
        $user = User::where('api_token', $request->input('api_token'))->get();
        $snippet = new Snippets();
        $snippet->snippet_name = $request->input('snippet_name');
        $snippet->user_id = $user[0]->id;
        $snippet->created_at = Carbon::now();
        $snippet->snippet = $request->input('snippet');
        $snippet->short_snippet = substr($request->input('snippet'), 0, 254);
        $snippet->tags = $request->input('tags');
        $snippet->plugin_upload = $request->input('plugin_upload');
        $snippet->save();
        return new SnippetResource($snippet);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewCount($id)
    {
        $viewCount = Viewcount::where('snippet_id', $id)->get();
        return $viewCount;
    }
}
