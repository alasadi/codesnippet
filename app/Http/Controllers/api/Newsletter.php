<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Models\User;

class Newsletter extends Controller
{
    public function Updateuser(Request $request)
    {
        $alluser = User::all();
    	$user = User::where('email', $request['email'])->first();
    	if ($user) {
    		$user->newsletter = 1;
    		$user->save();
    		return $user;
    	}

    	$token = bin2hex(openssl_random_pseudo_bytes(32));

        if (User::where('user_token', $token)) {
            $token = bin2hex(openssl_random_pseudo_bytes(32));
        }

    	$user = new User;
    	$user->email = $request['email'];
        $user->password = '';
        $user->registration_date = date('Y-m-d h:m:s');
        $user->api_token = $token; 
    	$user->newsletter = 1;
        $user->active = 1;
        $user->save();

        $data = array('name' => 'codesnippet', 'token' => $token);

        // Mail::send('mailtemplate.registrationMail', $data, function($message) use($request)
        // {
        //      $message->to($request['email'])
        //      ->subject('Codesnippet: Thanks for your registration!');
        // });

        return $user;

    }
}
