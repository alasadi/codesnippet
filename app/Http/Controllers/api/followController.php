<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Userprofile;
use App\Models\Follower;

use App\Http\Resources\FollowerResource;
use App\Http\Resources\MyFollowersResource;

class followController extends Controller
{
 	public function followers(Request $request)
    {
        $user = Userprofile::find($request['id']);
        $user = $user->followers;
        return FollowerResource::collection($user);
    }
    
    public function following(Request $request)
    {
        $user = Userprofile::find($request['id']);
        $user = $user->followings;
        return FollowerResource::collection($user);
    }

    public function myFollowers(Request $request)
    {
        $myFollowers = Follower::where("user_id", $request['id'])->get();
        return MyFollowersResource::collection($myFollowers);
    }

    public function followPost(Request $request)
    {
        $user = Userprofile::findOrFail($request['id']);
        $authUser = UserProfile::findOrFail($request['authid']);

        $user->followers()->attach($request['authid']);

        return MyFollowersResource::collection($authUser->followers);
    }
    public function unfollowPost(Request $request)
    {
        $user = Userprofile::findOrFail($request['id']);

        $user->followers()->detach($request['authid']);
        return MyFollowersResource::collection($user->followers);
    }

}
