<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Userprofile;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    //sending email in trait RegistersUsers
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return App\Models\User
     */
    protected function create(Request $request)
    {
        return redirect()->redirectTo('/login');
    }

    public function showRegister()
    {
        if (isset($_GET['token'])){
            $user = User::where('api_token', $_GET['token'])->firstOrFail();
            if ($user->password == '') {
                return view('auth.registration', ['user'=> $user]);
            }
            else{
                return response()->redirectToRoute('login');
            }
        }
        elseif (empty($_GET) || !isset($_GET['token'])) {
            return response()->redirectTo('register');
        }
    }

    public function registerWithPassword(Request $request)
    {
        $msg = $this->validate($request, [
            'firstname' => 'min:3',
            'lastname' => 'min:3',
            'password' => 'required|min:6|confirmed'

        ]);
        $user = User::all()->where('email', $request['email'])->first();
        $user->password = bcrypt($request['password']);
        $user->active = 1;
        $user->save();

        $profile = new Userprofile;
        $profile->insert([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'website' => $request->website,
            'work_at' => $request->work_at,
            'position' => $request->position,
            'qoute' => $request->qoute,
            'profile_pic' => $request->profile_pic,
            'status_id' => 1,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'linkedin' => $request->linkedin,
            'github' => $request->github,
            'about_me' => $request->about_me,
            'user_id' => $user->id
        ]);

        $credentials = ['email' => $user->email, 'password' => $user->password];
        $credentials = ['email' => $request['email'], 'password' => $request['password']];

        if (auth()->attempt($credentials)) 
        {
            return response()->redirectToRoute('index')->with('info', 'You Logged In Successfully, Welcome!');
        }
        else
        {
            return response()->redirectToRoute('login')->with('danger', 'Credentials doesnt match');
        }
    }
}




