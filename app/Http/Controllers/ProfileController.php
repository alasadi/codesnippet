<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Userprofile;
use App\Models\User;
use App\Models\Snippets;
use App\Models\Follower;
use Storage;



class ProfileController extends Controller
{
    public function Profile($id){
    	$profile = Userprofile::findOrFail($id);
        $user = User::findOrFail($id);
        
        $myFollowers = Follower::where("follower_id", auth()->id())->get();

    	$snippets = Snippets::where('user_id', $id)->where('snippet_status', 1)->get();

        return view('sites.profile', compact('profile', 'user', 'snippets', 'myFollowers'));
    }

    public function ProfileEditGet($id)
    {
        $profile = Userprofile::findOrFail($id);

        return view('sites.editprofile', compact('profile'));
    }

    public function ProfileEdit(Request $request, $id)
    {

        $profile = Userprofile::findOrFail($id);

        if ($request->hasFile('imgupload')) {
            //update pic
            $image = $request->file('imgupload');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $request->imgupload->move(public_path('images/'), $filename);
            $oldfilename = $profile->profile_pic;

            $profile->profile_pic = 'images/'.$filename;
            //delete old pic
            Storage::delete($oldfilename);
        }

    	$profile->fill($request->all());

    	$profile->save();

    	return redirect()->route('sites.profile', $profile->id);
    }
}