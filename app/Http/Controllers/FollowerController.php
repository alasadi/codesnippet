<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Userprofile;

class FollowerController extends Controller
{
    public function showFollowers($id)
    {
        $user = Userprofile::find($id);
        
        return view('sites.followers', ['user' => $user]);
        
    }
    public function showFollowing($id)
    {
        $user = Userprofile::find($id);
        
        return view('sites.showFollowing', ['user' => $user]);
    }
    public function followUser(int $profileId)
    {
        $user = Userprofile::find($profileId);
        if(!$user) {
         return redirect()->back()->with('error', 'User does not exist.'); 
        }
       
        $user->followers()->attach(auth()->user()->id);
        return redirect()->back()->with('success', 'Successfully followed the user.');
    }
    public function unFollowUser(int $profileId)
    {
        $user = Userprofile::find($profileId);
        if(!$user) {
            return redirect()->back()->with('error', 'User does not exist.'); 
        }

        $user->followers()->detach(auth()->user()->id);
        return redirect()->back()->with('info', 'Successfully unfollowed the user.');
    }
}
