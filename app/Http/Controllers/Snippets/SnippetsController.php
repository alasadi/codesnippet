<?php

namespace App\Http\Controllers\Snippets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\Models\Snippets;
use App\Models\Languages;
use App\Models\Tags;
use App\Models\Publication;
use App\Models\ViewCount;



class SnippetsController extends Controller
{
    public function Snippets()
    {
        $snippets = Snippets::where('snippet_status', 1)->paginate(10);
        if ($snippets->count()>0) {
            $langs = Languages::where('id', $snippets[0]->language_id)->get();
            return view('sites.snippets', ['snippets' => $snippets, 'langs' => $langs]);
        }
        return view('sites.snippets', ['snippets' => $snippets]);
    }

    public function meineSnippets()
    {
    	$snippets = Snippets::orderBy('id', 'desc')->where('user_id', Auth::user()->id)->get();
        if ($snippets->count() > 0) 
        {
            $langs = Languages::where('id', $snippets[0]->language_id)->get();
            return view('sites.meineSnippets', ['snippets' => $snippets, 'langs' => $langs]);
        }
    	return view('sites.meineSnippets', ['snippets' => $snippets]);
    }

    public function showSnippet($id)
    {
        $snippet = Snippets::findOrFail($id);
        $tags = $snippet->tags;
        $lang = Languages::findOrFail($snippet->language_id);

        if (Str::contains($tags, ' ')) 
        {
            $tags = explode(" ", $tags);
        }

        $is_viewed = ViewCount::where('userip', \Request::getClientIp(true))->where('snippets_id', $id)->get();
        if ($is_viewed->count() > 0) {
            return view('sites.showSnippet', ['snippet' => $snippet, 'tags' => $tags, 'lang' => $lang]);
        }

        $view = new ViewCount;
        $view->userip = \Request::getClientIp(true);
        $view->snippets_id = $id;
        $view->save();

        return view('sites.showSnippet', ['snippet' => $snippet, 'tags' => $tags, 'lang' => $lang]);
    }

    public function editSnippet($id)
    {
        $snippet = Snippets::findOrFail($id);
        $tags = $snippet->tags;
        $langs = Languages::all();
        $pub = Publication::all();

    	return view('sites.editSnippet', ['snippet' => $snippet, 'langs' => $langs, 'tags' => $tags, 'pub' => $pub]);
    }

    public function updateSnippet(Request $request, $id)
    {
        $this->validate($request, [
            'snippetname' => 'required|string', 
            'snippet' => 'required',
        ]);

        $snippet = Snippets::findOrFail($id);

        if ($request['uploadtxt']) 
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            ]);

            $destinationPath = "upload/";

            $file = $request['uploadtxt'];
            $file->move(base_path($destinationPath), $file->getClientOriginalName());
            $filePath = $destinationPath.$file->getClientOriginalName();
            $fileContent = file_get_contents('../'.$filePath);

            $snippet->snippet = $fileContent;
        }
        else
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            'snippet' => 'required',
            ]);

            $snippet->snippet = $request['snippet'];
        }

        $snippet->snippet_name = $request['snippetname'];
        $snippet->user_id = Auth::user()->id;
        $snippet->language_id = $request['language'];
        $snippet->created_at = Carbon::now();
        $snippet->short_snippet = substr($request['snippet'], 0, 254);
        $snippet->snippet_status = $request['is_public'];
        $snippet->tags = $request['tags'];
        $snippet->save();

        if ($request['uploadtxt']) 
        {
            unlink("../$filePath");
        }
        
        return redirect()->route('sites.meineSnippets');   
    }

    public function createSnippet(Request $request)
    {
        $snippet = new Snippets;
        $snippet->fill($request->old());
        $snippet->plugin = 0;
        $langs = Languages::all();
        $pub = Publication::all();
        return view('sites.createSnippet', ['snippet' => $snippet, 'langs' => $langs, 'pub' => $pub]);
    }

    public function storeSnippet(Request $request)
    {
        $snippet = new Snippets;
        $tags = new Tags;

        if ($request['uploadtxt']) 
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            ]);

            $destinationPath = "upload/";

            $file = $request['uploadtxt'];
            $file->move(base_path($destinationPath), $file->getClientOriginalName());
            $filePath = $destinationPath.$file->getClientOriginalName();
            $fileContent = file_get_contents('../'.$filePath);

            $snippet->snippet = $fileContent;
        }
        else
        {
            $this->validate($request, [
            'snippetname' => 'required|string', 
            'snippet' => 'required',
            ]);

            $snippet->snippet = $request['snippet'];
        }
            $snippet->snippet_name = $request['snippetname'];
            $snippet->user_id = Auth::user()->id;
            $snippet->language_id = $request['language'];
            $snippet->created_at = Carbon::now();
            $snippet->short_snippet = substr($request['snippet'], 0, 254);
            $snippet->snippet_status = $request['is_public'];
            $snippet->tags = $request['tags'];
            $snippet->save();

            if ($request['uploadtxt']) 
            {
                unlink("../$filePath");
            }
        return redirect()->route('sites.meineSnippets');   

    }

    public function deleteSnippet($id)
    {
        $snippet = Snippets::findOrFail($id);
        $snippet->viewcount()->delete();
        $snippet->delete();
        return redirect()->route('sites.meineSnippets');   
    }

    public function search(Request $request)
    {
        $snippets = Snippets::all();
        $searchVal = $request['search'];
        
        if ($snippets->count() > 0) {
            $langs = Languages::where('id', $snippets[0]->language_id)->get();
            
            if (Str::contains($searchVal, 'my Snippets:'))
            {
                if (auth()->check()) 
                {
                    $mySnippetsVal = explode(": ", $searchVal);
                    $snippets = Snippets::where('snippet_name', 'like', "%$mySnippetsVal[1]%")->get();

                    return view('sites.results', ['snippets' => $snippets, 'langs' => $langs]);
                }
                else
                {
                    return view('auth.login');

                }
            }
            elseif (Str::contains($searchVal, 'Language:'))
            {
                $mySnippetsVal = explode(": ", $searchVal);
                $searchLang = Languages::where('language_name','LIKE' , $mySnippetsVal[1])->get();
                $snippets = Snippets::where('language_id',  $searchLang[0]->id)->get();

                return view('sites.results', ['snippets' => $snippets, 'langs' => $langs]);
            } 
            elseif (str_contains($_GET['search'], 'tags:')) 
            {
                $mySnippetsVal = explode(": ", $_GET['search']);
                $snippets = Snippets::where('tags', 'like', "%$mySnippetsVal[1]%")->get();
                return view('sites.results', ['snippets' => $snippets]);
            }
            else
            {
                $snippets = Snippets::where('snippet_name', 'LIKE', "%$searchVal[1]%")->where('snippet_status', 1)->get();

                return view('sites.results', ['snippets' => $snippets, 'langs' => $langs]);
            }
        }
        else
        {
           return view('sites.snippets', ['snippets' => $snippets]);
        }
    } 

}


