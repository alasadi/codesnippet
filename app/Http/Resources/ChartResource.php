<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'snippet_id' => $this->snippet,
            'snippetname' => $this->snippet_name,
            'id' => $this->id,
            'count' => $this->viewcount->count()
        ];
    }
}
