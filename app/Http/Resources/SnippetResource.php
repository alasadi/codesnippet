<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SnippetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'snippet_name' => $this->snippet_name,
            'snippet' => ltrim($this->snippet, "\r\n"),
            'short_snippet' => ltrim($this->short_snippet, '\r\n'),
            'language_id' => 1,
            'user_id' => $this->user_id,
            'tags' => $this->tags,
            'snippet_status' => $this->snippet_status,
            'created_at' => $this->created_at,
            'short_snippet' => substr($this->snippet,  0, 254),
            'user' => [
                'id' => $this->user->id,
                'firstname' => $this->user->firstname,
                'lastname' => $this->user->lastname,
                'profile_pic' => $this->user->profile_pic,
            ],
            'viewcount' => $this->viewcount->count(),
            'language' => $this->lang->language_name
        ];
    }
}
