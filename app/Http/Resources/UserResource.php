<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
        [
            'id' => $this->id,
            'firstname' => isset($this->profile->lastname) ? $this->profile->firstname : '',
            'lastname' => isset($this->profile->lastname) ? $this->profile->lastname : '',
            'loggedin' => $this->loggedin,
            'type' => $this->type,
            'registration_date' => $this->registration_date
        ];
    }
}
