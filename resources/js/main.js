$(document).ready(function(){

  //toggle nav dropdown
  $(".dropdown").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(".dropdown-list").toggle();
  });
  $(document).click(function(e){
    if (!$(e.target).is(".dropdown") || !$(e.target).is(".dropdown-list a")) {
      $(".dropdown-list").hide();
    }
  })
  // end toggle nav dropdown

  //delete snippet confirmation
  $('.btn.delete').on('click', function(){
    var conf = confirm('Do you want to delete your Snippet');

    if (conf == true) {
      return true;
    }else{
      return false;
    }
  });
  //end delete snippet confirmation
//toggle mobiledropdown
  $(".mobile a").on("click", function(e){
    e.preventDefault();

    $(".mobilenav").toggle();
  });
//end toggle mobiledropdown


  //tooltip views
  $('.viewcount').on('mouseover', function(){
    $('.tooltip_views').find('.tooltiptext').css("visibility", "visible");

  });
  $('.viewcount').on('mouseleave', function(){
    $('.tooltip_views').find('.tooltiptext').css("visibility", "hidden");

  });

  setTimeout(function(){
    $('.alert').animate({
      'top': -350,
      'display': 'none'
    })
  }, 1300)

  $(".toggle").click(function (e) {
    e.stopPropagation();
    jQuery(this).children('.toggle').toggle();
});

})