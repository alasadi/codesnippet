var Vue = require('vue');

import VueClipboard from 'vue-clipboard2';


Vue.use(VueClipboard)

Vue.config.productionTip = false

 
Vue.component(
	'snippets', require('./components/Snippets.vue')
)

Vue.component(
	'follower', require('./components/Follower.vue')
)
Vue.component(
	'copy', require('./components/Copy.vue')
)
Vue.component(
	'mysnippets', require('./components/Mysnippets.vue')
)
Vue.component(
	'newsletter', require('./components/Newsletter.vue')
)
Vue.component(
	'charts', require('./components/UploadedCharts.vue')
)



new Vue({
 el: '#app'
});