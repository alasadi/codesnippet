@extends('layout.master')
@section('title', 'Home')
@section('wrapper')
@section('header')
<div class="heroimg">
	<div class="heroimg-content">
		<h1>
			Upload and reuse your own Code.
		</h1>
		@guest
			<div class="home-lregister"> 
				<a href="{{route('login')}}" class="loginbtn btn">Login</a><br>
				<a href="{{route('register')}}" class="registerbtn btn ">Register</a>
			</div>
		@else
			<div class="home-lregister"> 
				<a href="{{route('sites.snippets')}}" class="loginbtn btn">See All Snippets</a><br>
				<a href="{{route('sites.meineSnippets')}}" class="registerbtn btn ">My Snippets</a>
			</div>
		@endguest
	</div>
</div>
@stop
@section('main')
	<section id="about">
		<div class="section-head">
			<h3>About Code Snippet</h3>
			<span class="line"></span>
		</div>
		<div class="section-body">
			<p>
				CodeSnippet is a Website where you can store your snippets to reuse them whenever you want and you don't even have to leave your Development Environment for it. If you install the CodeSnippet-Plugin on your Editor you can just mark and upload your code with one key sequences.
			</p>
		</div>
	</section>
	<section id="benefits">
		<div class="section-head">
			<h3>Benefits</h3>
			<span class="line"></span>
		</div>
		<div class="section-body">
			<ul class="benefits">
				<li>
					<div class="benefits-header">
						<img src="{{asset('img/img-1.png')}}" alt=""><br>
						<strong>Web</strong>
					</div>
					<div class="benefits-body">
						<p>Interact with other Users follow them and see what they upload. </p>
					</div>
				</li>
				<li>
					<div class="benefits-header">
						<img src="{{asset('img/img-2.png')}}" alt=""><br>
						<strong>Plugin</strong>
					</div>
					<div class="benefits-body">
						<p>
							Upload your own Snippet without leaving your Development Envoirment after downloading our plugin in your editor. 
						</p>
					</div>
				</li>
				<li>
					<div class="benefits-header">
						<img src="{{asset('img/img.png')}}" alt=""><br>
						<strong>Search</strong>
					</div>
					<div class="benefits-body">
						<p>Searching for your Snippets/Languages/Tags or Snippet names from every Page of the Website</p>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<section id="howtouse">
		<div class="section-head">
			<h3>How to Use Code Snippet</h3>
			<span class="line"></span>
		</div>
		<div class="section-body">
			<p>
				Create a Profile and download our Plugin and start uploading your Snippets directly from your Editor. read more <a href="{{route('sites.howto')}}">here</a>
			</p>
		</div>
	</section>
	<section id="signup" class="signup">
		<newsletter></newsletter>
	</section>
</main>
@stop
