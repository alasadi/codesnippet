@extends('layout.master')
@section('title', 'Snippet detail')
@section('wrapperclass', 'snippet-detail')
@section('main')
<section class="detail">
	<h1>
		{{$snippet->snippet_name}}
	</h1>
	<small>@isset($snippet->user->email) {{$snippet->user->email}}, {{$snippet->created_at->format('Y-m-d')}} @endisset</small> <br> <br>
	<small>Language: </small> <small>{{$snippet->lang->language_name}}</small>

	<div class="code">
		<pre>
			<code class="language-{{$lang->language_name}}">{{$snippet->snippet}}
			</code>
		</pre>
	</div>
	@if($tags)
	<div class="tags">
		@if(is_array($tags))
			@foreach($tags as $tag)
			<p><a href="{{route('searchTag', $tag) }}" title="search for {{$tag}}"> {{$tag}}</a></p>
			@endforeach
		@else
			<p><a href="{{route('searchTag', $tags)}}" title="search for {{$tags}}"> {{$tags}}</a></p>
		@endif
	</div>
	@endif
	
	<copy snippetid="{{$snippet->id}}"></copy>

	<div class="tooltip tooltip_views">
      <span class="tooltiptext">Views</span>
    </div>
    <div class="view viewcount"> <span>{{$snippet->viewcount->count()}}</span>
        <i class="far fa-eye" title="views"></i>
    </div>
    @isset($snippet->user->id)
	@if(auth()->check() && auth()->id() == $snippet->user->id)
		@include('sites._edit')
	@endif
	@endisset
</section>
@endsection