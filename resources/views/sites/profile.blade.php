@extends('layout.master')
@section('title', $profile->firstname . "'s Profile")
@section('wrapperclass', 'user-profile')
@section('main')
<div class="pf-wrapper">
  <div class="pf-head"> 
    <div class="head-left">
      <img src='{{$profile->profile_pic ? (strpos($profile->profile_pic, "images/") !== false ? "../$profile->profile_pic" : $profile->profile_pic) : "../images/dummy-img.png"  }}' alt="" width="170" height="170">
      <div class="head-followers">
        <follower></follower>
      </div>

      
    </div>
    <div class="head-txt">
      <h1>{{$profile->firstname}} {{$profile->lastname}}</h1>
      @isset($profile->position)
        <span>{{$profile->position}} at</span><br>
        <span>{{$profile->work_at}}</span><br>
        <q>
          <i>{{$profile->qoute}}</i>
        </q> 
      @endisset
    </div>
  </div>
  <div class="between links">
    <a href="{{$profile->github}}" title="{{$profile->firstname}}'s Github"> <i class="fab fa-github-square"></i></a>
    <a href="{{$profile->facebook}}" title="{{$profile->firstname}}'s Facebook"><i class="fab fa-facebook"></i></a>
    <a href="{{$profile->twitter}}" title="{{$profile->firstname}}'s Twitter"><i class="fab fa-twitter-square"></i></a>
    <a href="{{$profile->linkedin}}" title="{{$profile->firstname}}'s LinkedIn"><i class="fab fa-linkedin"></i></a>
    <a href="{{$profile->website}}" title="{{$profile->firstname}}'s Website"><i class="fas fa-globe"></i></a>
  </div>
  @if(auth()->user()->id == $user->id)
  <div class="tokenwrapper">
    <h2>Token to upload your Code from your Development envoirment:</h2>
    <span class="token">{{$user->api_token}}</span><br>
    <small>More infos about uploading your code <a href="{{route('sites.howto')}}">here</a></small>
  </div>
  @endif
  <div class="wrapper-body">
    <div class="columns">
      <div class="column info">
        <h3>About me...</h3>
        <div class="about">
          <p>
            {{$profile->about_me}}
          </p>
        </div>
      </div>

    </div>
  </div>
  <div class="pf-snippets">
    
     <mysnippets mysnippets="false"></mysnippets>
   
  </div> 
  <div class="follower">
    <div class="f-users">
      
     <follower></follower>
    
    </div>
   
  </div>
</div>
@endsection