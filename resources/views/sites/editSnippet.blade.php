@extends('layout.master')
@section('title', 'Edit your snippet')
@section('wrapperclass', 'snippet-detail')
@section('main')
	<section class="upload">
	<h1>Edit this Snippet</h1>
	<form method="post" action="{{route('sites.updateSnippet', $snippet->id)}}">
		@csrf
		@method('post')
		@include('sites._form')
	</form>
</section>
@endsection