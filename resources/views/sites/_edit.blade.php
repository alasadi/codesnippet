<div class="btns">
	<a href="{{route('sites.editSnippet', $snippet->id)}}" class="more btn"><i class="fas fa-edit"></i></a>
	<form method="post" action="{{route('sites.deleteSnippet', $snippet->id)}}">
		@csrf
		<button type="submit" class="more btn delete"><i class="fas fa-trash-alt"></i></button>
	</form>
</div>