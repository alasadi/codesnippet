<h1 class="results">
	{{count($snippets) }}
      {{(count($snippets) != 1  ? 'Snippets': 'Snippet')}} 
</h1>
<ul class="meine_snippets-list">

	@foreach($snippets as $snippet)
	<li>
		<div class="snippet-body">
			<h3>
				{{$snippet->snippet_name}}
			</h3>
			<small>{{$snippet->user->email}}, {{$snippet->created_at->format('Y-m-d')}}</small>
				<a href="{{route('sites.showSnippet', $snippet->id)}}">
					<pre><code class="language-{{isset($langs[0]) ? $langs[0]->language_name : ''}}"> @{{ $snippet->short_snippet}}</code></pre> <!-- dont make a break after pre or code -->
				</a>
				@include('sites._edit')
		</div>
	</li>
	@endforeach
</ul>