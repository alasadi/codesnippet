@extends('layout.master')
@section('title', 'All Snippets')
@section('wrapperclass', 'snippets')
@section('main')
	@include('sites._snippets')
@endsection