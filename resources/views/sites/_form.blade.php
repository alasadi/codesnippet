<div class="snippetname">
	<input type="text" placeholder="Snippet Name" value="{{$snippet->snippet_name}}" name="snippetname">
</div>
<div class="code">
	<textarea placeholder="Code" name="snippet">{{urldecode($snippet->snippet)}}</textarea>
</div>
<div class="code file">
	<span>OR</span> <input type="file"><br>
	<small>uploading a file will overwrite the code in the Textarea</small>
</div>
<div class="language">
	<select name="language" id="">
		@foreach($langs as $lang)
			<option value="{{$lang->id}}" {{$lang->id == $snippet->language_id ? 'selected' : ''}}>{{$lang->language_name}}</option>
		@endforeach
	</select>
</div>
<div class="is_public">
	<select name="is_public">
		@foreach($pub as $pub)
			<option value="{{$pub->id}}" selected="{{$pub->id == $snippet->snippet_status ? 'selected' : ''}}">{{$pub->publications}}</option>
		@endforeach
	</select>
</div>
<div class="tags">
	<input type="text" placeholder="Tags" name="tags" value="{{isset($tags) ? $tags : ''}}"> 
</div>
<button type="submit">Submit Your Code</button>