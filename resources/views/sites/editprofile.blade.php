@extends('layout.master')
@section('wrapperclass', 'snippets meine-snippets')
@section('title', 'Edit your Profile')
@section('main')
<div class="pf-wrapper">
  <h3>My profile</h3>
  <div class="profile-wrapper profile">

    <div class="profile-dates">
      <form action="{{route('profilepost', $profile->id)}}" method="post" enctype="multipart/form-data">
        @method('post')
        @csrf
            <span class="profile-img">
              <img  src='{{$profile->profile_pic ? (strpos($profile->profile_pic, "images/") !== false ? "../../$profile->profile_pic" : $profile->profile_pic) : "../../images/dummy-img.png"  }}' alt="" style="width:200px; height: 200px;">
              <input type="file" name="imgupload" id="imgupload" value="" style="display: none"> 
              <label for="imgupload" class="uploadimg"><i class="fas fa-camera"></i></label>
            </span>
            
        <div class="firstname">
          <label for="firstnam">Firstname</label> <br>
          <input type="text" placeholder="firstname" name="firstname" value="{{$profile->firstname}}">
        </div>
        <div class="lastname">
          <label for="lastname">Lastname</label> <br>
          <input type="text" placeholder="lastname" name="lastname" value="{{$profile->lastname}}">
        </div>
        <div class="email">
          <label for="email">E-Mail</label> <br>
          <input type="email" placeholder="email" name="email" value="{{$profile->email}}">
        </div>
        <div class="password">
          <label for="password">Password</label> <br>
          <input type="password" placeholder="password" name="password" value="{{$profile->password}}">
        </div>
        <div class="password">
          <label for="password">Confirm Password</label> <br>
          <input type="password" placeholder="password" name="password_confirmation" value="{{$profile->password}}">
        </div>
        <div class="website">
          <label for="website">Website</label> <br>
          <input type="text" placeholder="Website" name="Website" value="{{$profile->Website}}">
        </div>
        <div class="socialmedia-twitter">
          <label for="twitter">Twitter</label> <br>
          <input type="text" placeholder="twitter" name="twitter" value="{{$profile->twitter}}">
        </div>
        <div class="socialmedia-facebook">
          <label for="Linkedin">Facebook</label> <br>
          <input type="text" placeholder="facebook" name="facebook" value="{{$profile->facebook}}"> 
        </div>
        <div class="socialmedia-linkedin">
          <label for="linkedin">Linkedin</label> <br>
          <input type="text" placeholder="linkedin" name="linkedin" value="{{$profile->linkedin}}">
        </div>
        <div class="qoute">
          <label for="Qoute">Qoute</label> <br>
          <input type="text" placeholder="qoute" name="qoute" value="{{$profile->qoute}}">
        </div>
        <div class="aboutme">
          <label for="aboutme">About me...</label> <br>
          <textarea name="aboutme" id="aboutme" placeholder="Tell the others something interesting about you">{{$profile->about_me}}</textarea>
        </div>
        <button type=submit>edit</button>
      </form>
    </div>
  </div>
</div>
@endsection