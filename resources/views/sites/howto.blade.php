@extends('layout.master')
@section('title', 'How to use')
@section('wrapperclass')
@section('main')
	<div class="howto" >	
		<h1>How To Use CodeSnippet!</h1>
		<div class="ps">
			<p>First of all: What is CodeSnippet and why you should use it. </p>
			<p>CodeSnippet is a Website where you can store your snippets to reuse them whenever you want and you don't even have to leave your Development Environment for it. If you install the CodeSnippet-Plugin on your Editor you can just mark and upload your code with one key sequences. </p>
			<p>
				<strong>Why should I use it, you ask yourself?</strong>
			</p>
			<p>
					Let's be honest: we all had to find some Functions in the internet to make our Projects work or to add it to our Projects and sometimes we need those Functions in a another one so we go and search for them again, right? With CodeSnippet you don't have to do that anymore! Just upload with one key sequence and look it up if needed. Done! Easy, Right?
			</p>
			<p>
				But That's not it! You also can use the code from other uploaders, as long as they make it public. If you find someone who uploaded many Snippets which are useful for you, you can follow him and get notifications whenever this User uploads something.
			</p>
			<p>
				Lets Talk about the Usage of the Application:
			</p>
			<ol>
				<li>
					<a href="#web">Web</a>
				</li>
				<li>
					<a href="#plugin">Plugin</a>
					<ul>
						<li>
							<a href="#sublime">Sublime</a>
						</li>
					</ul>
				</li>
			</ol>
			<h3 id="web">Web:</h3>
			<p style="padding-left: 30px;">
				After you have registered to Code Snippet and validated your EMail Address(don't forget to complete your Profile, you'll get logged in and from there you have many Options on what to do on the Website.
			</p>
			<p style="padding-left: 60px;">1. Profile:</p>
			<p style="padding-left: 90px;">
				Here You can see your own Profile and edit it however you want. If you click on Followers/Following, you will see your Followers or the Users you are following. In the middle you can see your Token, which is only available for you. More about it <a href="#plugin">here</a>.
			</p>
			<p style="padding-left: 60px;">2. My Snippets</p>
			<p style="padding-left: 90px;">
				In <strong>My Snippets</strong> you can Manage your own Snippets, upload a new one delete an old one or copy it with one click and paste it in your Project.
			</p>
			<p style="padding-left: 60px;">3. All Snippets</p>
			<p style="padding-left: 90px;">
				Here you can see all Snippets which are public and if you find a useful one, go ahead and copy it.
			</p>
			<p style="padding-left: 60px;">4. Search</p>
			<p style="padding-left: 90px;">
				Here you have many possiblities how to use the Search function:
			</p>
			<p style="padding-left: 90px;">
				1. Search in all Snippets: just write whatever you are looking for and we will provide you the Snippets that match your search.
			</p>
			<p style="padding-left: 90px;">
				2.&nbsp;Search in My Snippets: if you want to Search in My Snippets from whereever you are just start your Search with <span >My Snippets:</span> from there we weill do the rest and provide you only your Snippets.
			</p>
			<p style="padding-left: 90px;">
				3. Search only Languages: If you are Looking for a Language e.g. PHP, start your search query with Language: and you will get only Snippets with the Language PHP.
			</p>
			<p style="padding-left: 90px;">
				4. Also you can search for tags. To find your Snippet faster just add as many Tags as you want. If other Users do the same and you are looking for the tag let say "Authentication" you will find all Snippets with the Tag Authentication and for sore you can search for more than one Tag in one query.
			</p>
			<h3 id="plugin">Plugin:</h3>
			<p style="padding-left: 30px;">
				After downloading Codesnippet Plugin for your Development Envoirment and set it up in few steps you are ready to upload your code without leaving your Editor. Let see how.
			</p>
			<p style="padding-left: 30px;">
				<strong>After downloading code snippet please follow the following instructions in order  for a proper use:</strong>
			</p>
			<p style="padding-left: 30px;">
				As we said before, everyuser has his own Token to upload his code. You find yours in your Profile.
			</p>
			<p style="padding-left: 60px;">
				<strong id="sublime">Sublime Text:</strong>
			</p>
			<p style="padding-left: 90px;">
				Preferences-&gt;Settings: in the User settings(right side) you have to add following:
			</p>
			<p style="padding-left: 90px;">
				"codesnippet_key": [ "YOUR TOKEN" ], thats all we need in this one. Save it and close it.
			</p>
			<p style="padding-left: 90px;">
				again to Preferences-&gt;key Bindings add this:
			</p>
			<p style="padding-left: 90px;">
				{ "keys": ["ctrl+shift+t"], "command": "codesnippet" }, { "keys": ["ctrl+shift+t"], "command": "codesnippet" },
			</p>
			<p style="padding-left: 90px;">
				For sure you can customize your keys how ever you want but make sure they are not used yet.
			</p>
		</div>
		<p>Thats it you are done.</p>
		<p>HAPPY CODING.</p>

	</div>
@endsection