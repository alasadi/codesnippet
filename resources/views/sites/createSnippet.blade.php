@extends('layout.master')
@section('title', 'create Snippet')
@section('main')
	<section class="upload">
		<h1>Create  Snippet</h1>
		<form method="post" action="{{route('sites.createSnippet')}}" enctype="multipart/form-data">
			@csrf
			@include('sites._form')
		</form>
	</section>
@endsection