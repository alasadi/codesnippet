@extends('layout.master')
@section('title', 'Search result')
@section('wrapperclass', 'snippets')
@section('main')
	@if($_GET['search'] == "my+Snippets")
		@include('sites._mySnippets')
	@else
		<h1 class="results">
			{{count($snippets)}} Snippets
		</h1>
		<ul class="snippets-list">
			@foreach($snippets as $snippet)
			<li>
				<div class="snippet-user">
					<a href="{{route('sites.profile', $snippet->user->id)}}">
						<img src="{{$snippet->user->profile_pic}}" alt="">
					</a>
					<div class="u-i">
						<a href="{{route('sites.profile', $snippet->user->id)}}">
							<span>{{$snippet->user->firstname}}</span><br>
						</a>
						<span><small>{{date('Y-M-D h:m', strtotime($snippet->created_at))}}</small></span>
					</div>
				</div>
				<div class="snippet-body-wrapper">
					<div class="snippet-body">
						<h3>
							{{$snippet->snippet_name}}
						</h3>
						<small class="mobile-snippet">{{$snippet->user->email}}, {{$snippet->created_at->format('Y-m-d')}}</small>
						<a href="{{route('sites.showSnippet', $snippet->id)}}">
							<pre><code class="language-{{isset($langs) ? $langs[0]->language_name : ''}}">{{$snippet->short_snippet}}</code>
							</pre>
						</a>
					</div>
					<div class="rating-m">
						<copy snippetid="{{$snippet->id}}"></copy>
						<div class="rates">
							<span>{{$snippet->viewcount->count()}}</span>
							<i class="far fa-eye"></i>
						</div>
					</div>
				</div>
				<div class="rating">
					<copy snippetid="{{$snippet->id}}"></copy>

					<div class="rates">
						<span>{{$snippet->viewcount->count()}}</span>
						<i class="far fa-eye"></i>
					</div>
				</div>
			</li>
			@endforeach
		</ul>		

	@endif
@endsection