

<div class="main">
  <div class="left-nav">
	<ul>
		<li>
			<div class="left-nav__cf"> 
		        <div class="left-nav__title" >
		          <a href="{{route('cms.charts')}}">Charts</a>
		        </div>
	      	</div>
		</li>
		<li>
			<div class="left-nav__cf"> 
		        <div class="left-nav__title" >
		          <a href="{{route('cms.users')}}">Users</a>
		        </div>
	      	</div>
		</li>
		<li>
			<div class="left-nav__cf"> 
		        <div class="left-nav__title" >
		          <a href="{{route('cms.snippets')}}">Snippets</a>
		        </div>
	      	</div>
		</li>

		<li>
			<div class="left-nav__cf"> 
		        <div class="left-nav__title">
		          <a href="{{route('cms.logout')}}">Logout</a>
		        </div>
	      	</div>
		</li>
		
	</ul>
      
    

  </div>
  <div class="content">
    @yield('cmscontent')
  </div>
</div>