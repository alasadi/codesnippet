<footer>
	<div class="copyright">&copy; Code Snippet 2019</div>
	<ul class="footer-list">
		<li><a href="#">Home</a></li>
		<li><a href="support.html">Support</a></li>
		<li><a href="terms.html">Terms</a></li>
		<li><a href="imprint.html">Imprint</a></li>
	</ul>
</footer>