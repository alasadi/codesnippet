<nav class="nav">
	<div class="nav-head">
		<div class="logo clearfix">
			<a href="{{route('index')}}"><img src="{{asset('img/logo.png')}}" alt="Logo"></a>
		</div>
		@guest
		<ul class="desktopnav gst">
			<li><a href="{{route('sites.snippets')}}">Snippets</a></li>
			
			<li><a href="{{route('sites.howto')}}">How To Use</a></li>
			<li>
				<form class="search" action="{{route('sites.results')}}" method="get">
					<input type="text" placeholder="Search..." name="search">
				</form>
			</li>
			<li><a href="{{route('login')}}">Login</a></li>
		</ul>
		@else
		<ul class="desktopnav ngst">
			<li><a href="{{route('sites.snippets')}}">Snippets</a></li>
			<li><a href="{{route('sites.meineSnippets')}}">Meine Snippets</a></li>
			<li><a href="{{route('sites.howto')}}">how to use</a></li>
			<li>
				<form class="search" action="{{route('sites.results')}}" method="get">
					<input type="text" placeholder="Search..." name="search">
				</form>
			</li>
			<li><a href="" class="dropdown"><i class="fas fa-user-circle prf"></i></a>
				<ul class="dropdown-list">
					<li><a href="{{route('sites.profile', auth()->user()->id)}}">Profile</a></li>
					<li><a href="{{route('logout')}}">Logout</a></li>
				</ul>
			</li>
		</ul>
		
		@endguest
		<div class="mobile">
			<a href=""><span></span></a>
		</div>
	</div>
	<ul class="mobilenav">
		<li><a href="{{route('sites.snippets')}}">Snippets</a></li>
		<li><a href="{{route('sites.meineSnippets')}}">Meine Snippets</a></li>
		<li><a href="{{route('sites.howto')}}">how to use</a></li>
		<li>
			<form class="search" action="{{route('sites.results')}}" method="get">
				<input type="text" placeholder="Search..." name="search">
			</form>
		</li>
		@guest
			<li><a href="{{route('login')}}">Login</a></li>
		
		@else
			<li><a href="{{route('sites.profile', auth()->user()->id)}}">Profile</a></li>
			<li><a href="{{route('logout')}}"></a>Logout</li>
		@endguest
	</ul>
</nav>