@extends('layout.master')

@section('main')
<div class="container">
            <div class="register-card">
                <div class="card-header"><h2>{{ __('Register') }}</h2></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div>
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div>
                            <button type="submit">
                                {{ __('Register') }}
                            </button> <br>
                            <a href="{{route('login')}}">Already Registered? Login Here</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
