@extends('layout.master')

@section('main')
<div class="container">
        <div class="registration">
            <div class="register-card">
                <div class="card-header"> <h2>{{ __('Register') }}</h2></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('auth.register') }}">
                        @csrf

                        <div>
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <div>
                                <input id="email" type="email"  value="{{ $user->email}}" readonly name="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- profile -->
                        <div>
                            <label for="firstname">{{ __('Firstname') }}</label>

                            <div>
                                <input id="lastname" type="text" name="firstname">
                            </div>
                        </div>
                        <div>
                            <label for="lastname">{{ __('Lastname') }}</label>

                            <div>
                                <input id="lastname" type="text" name="lastname">
                            </div>
                        </div>
                        <div>
                            <label for="website">{{ __('Website') }}</label>

                            <div>
                                <input id="website" type="text" name="website">
                            </div>
                        </div>
                        <div>
                            <label for="work_at">{{ __('Where do you Work?') }}</label>

                            <div>
                                <input id="work_at" type="text" name="work_at">
                            </div>
                        </div>

                        <div>
                            <label for="position">{{ __('Whats your Position in the Company?') }}</label>

                            <div>
                                <input id="position" type="text" name="position">
                            </div>
                        </div>
                        <div>
                            <label for="qoute">{{ __('Do you want to add a Qoute?') }}</label>

                            <div>
                                <input id="qoute" type="text" name="qoute">
                            </div>
                        </div>
                        <div class="socialmedia">
                            <strong>Social Media</strong>
                        </div>
                        <div>
                            <label for="facebook">{{ __('Facebook') }}</label>

                            <div>
                                <input id="facebook" type="text" name="facebook">
                            </div>
                        </div>
                        <div>
                            <label for="twitter">{{ __('Twitter') }}</label>

                            <div>
                                <input id="twitter" type="text" name="twitter">
                            </div>
                        </div>
                        <div>
                            <label for="linkedin">{{ __('LinkedIn') }}</label>

                            <div>
                                <input id="linkedin" type="text" name="linkedin">
                            </div>
                        </div>
                        <div>
                            <label for="github">{{ __('Github') }}</label>

                            <div>
                                <input id="github" type="text" name="github">
                            </div>
                        </div>
                        <div>
                            <label for="about_me">{{ __('Tell the people somthing about you') }}</label>

                            <div>
                                <textarea id="about_me" type="text" name="about_me"></textarea>
                            </div>
                        </div>

                        <!-- //profile -->
                        <div >
                            <label for="password" >{{ __('Password') }}</label>

                            <div >
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div >
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>

                            <div >
                                <input id="password-confirm" type="password" name="password_confirmation" required>
                            </div>
                        </div>

                        <div >
                            <div >
                                <button type="submit">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
