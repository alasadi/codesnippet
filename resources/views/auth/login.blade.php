@extends('layout.master')

@section('main')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <div class="login">
                <div class="card-header"><h2>{{ __('Login') }}</h2></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div>
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <div>
                                <input id="email" type="email" name="email">

                            </div>
                        </div>

                        <div>
                            <label for="password">{{ __('Password') }}</label>

                            <div >
                                <input id="password" type="password" name="password" required>
                            </div>
                        </div>
                         <div style="margin: 10px 0 20px 0">
                            <input type="checkbox" class="form-check-input" id="checkRememberMe" name="remember_me" style="float:left; width:5%">
                             <label for="checkRememberMe">Remember me</label>
                            
                           
                        </div>
                        <div>
                            <div>
                                <button type="submit">
                                    {{ __('Login') }}
                                </button>
                                
                            </div>
                            <div><br>
                                <a href="{{route('register')}}">New User? Register Here</a>
                            </div>
                            <!-- <div><br>
                                <a href="{{route('password.request')}}">Forget Password?</a></div> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection