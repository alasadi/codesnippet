<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="id" content="{{ isset($user) ? $user->id : '' }}">
	<meta name="auth-id" content="{{ auth()->check() ? auth()->user()->id : 'guest' }}">
	<title>CodeSnippet - @yield('title')</title>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/prism.css')}}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css">
</head>
<body>
	<div class="wrapper @yield('wrapperclass')" id="app">
		@yield('wrapper')
		<header class="header">
		@include('partials.nav')
        @include('partials.alerts')
			
		@yield('header')

		</header>
		<main>
			@yield('main')
		</main>
		@include('partials.footer')
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="{{asset('js/main.js')}}"></script>
  <script src="{{asset('js/app.js')}}"></script>
  <script src="{{asset('js/prism.js')}}"></script>

</body>

</html>