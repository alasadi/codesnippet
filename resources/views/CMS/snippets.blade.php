@extends('layout.cmsMaster')
@section('wrapperclass', 'snippets meine-snippets')
@section('title', 'CMS Snippets')
@section('cmscontent')
  	<table>
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Viewed</th>
	      <th>Language</th>
	      <th>Created At</th>
	      <th>Name</th>
	      <th>Edit</th>
	    </tr>
	  </thead>
	  <tbody>
      @foreach($snippets as $snippet)
	    <tr>
	      	<td> {{$snippet->id}}</td>
	      	<td>{{$snippet->viewcount->count()}}</td>
	      	<td>{{$snippet->lang->language_name}}</td>
	      	<td>{{date('Y-M-d h:m', strtotime($snippet->created_at))}}</td>

	      	<td>{{$snippet->snippet_name}}</td>

	      	<td>
	      		<a href="{{route('cms.snippetdetail', $snippet->id)}}" title="Edit User">
	      			<i class="fas fa-edit"></i>
	      		</a>
      			<form  method="post" action="{{route('cms.deleteSnippet', $snippet->id)}}" onclick="return deletesnippet()">
      				@csrf
		      		<button type="submit" title="Delete User">
		      		 	<i class="fas fa-trash-alt"></i>
		      		</button>
	      		</form>

	      	</td>
	    </tr>
	      @endforeach

	  </tbody>
	</table>
	<script>
		function deletesnippet() {
			if (confirm('do you want to delete this snippet?')) {
				return true;
			}
			return false;
		}
	</script>
@endsection