@extends('layout.cmsMaster')
@section('wrapperclass', 'snippets meine-snippets')
@section('title', 'CMS Users')
@section('cmscontent')
<div style="float:right;">
  <form action="{{route('cms.deleteUser', $user->id)}}" method="post" style="width:100%">        
    <button type="submit" class="deleteUser">
          Delete User
    </button>
  </form>
</div>
  <form action="{{route('cms.usersPost', $user->id)}}" method="post" class="userdetails-form">
    @csrf
  <div>
      <label >
        <p>Firstname</p>
        <input type="text" value="{{$user->profile->firstname}}" name="firstname" />
      </label>
      <label >
        <p>Lastname</p>
        <input type="text" value="{{$user->profile->lastname}}"  name="lastname"/>
      </label>
      <label >
        <p>E-Mail</p>
        <input type="text" required value="{{$user->profile->email}}"  name="email"/>
      </label>

      <label>
        <p>User-Type</p>
        <input type="text" value="{{$user->type}}" required=""  name="type"/>
      </label>
      <label>
        <p>Works at</p>
        <input value="{{$user->profile->work_at}}"  name="work_at"/>
      </label>
      <label>
          <p>Website</p>
        <input value="{{$user->profile->website}}"  name="website"/>
      </label>
      <label>
        <p>About me</p>
        <textarea name="about_me">
          {{$user->profile->about_me}}
        </textarea>
      </label>
    </div>
    <div>
      <label>
          <p>Facebook</p>
        <input value="{{$user->profile->postition}}" name="facebook" />
      </label>
      <label>
          <p>Qoute</p>
        <input value="{{$user->profile->qoute}}"  name="qoute"/>
      </label>
      <label>
          <p>Twitter</p>
        <input value="{{$user->profile->twitter}}"  name="twitter"/>
      </label>
      <label>
          <p>LinkedIn</p>
        <input value="{{$user->profile->linkedin}}"  name="linkedin"/>
      </label>    
      <label>
          <p>Github</p>
        <input value="{{$user->profile->github}}"  name="github"/>
      </label>
      <label>
          <p>Password(Hashed)</p>
        <input type="password" value="{{$user->password}}" required  name="password"/>
      </label>
      <label>
          <p>Confirm Password</p>
        <input type="password"/>
      </label>
        <button type="submit">
          Submit
        </button>
    </div>
  </div>
  </form>

@endsection