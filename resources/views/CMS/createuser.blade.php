@extends('layout.cmsMaster')
@section('wrapperclass', 'snippets meine-snippets')
@section('title', 'Create User')
@section('cmscontent')

  <form action="{{route('cms.createUserPost')}}" method="post" class="userdetails-form">
    @csrf
  <div>
      <label >
        <p>Firstname</p>
        <input type="text" value="" name="firstname" />
      </label>
      <label >
        <p>Lastname</p>
        <input type="text" value=""  name="lastname"/>
      </label>
      <label >
        <p>E-Mail</p>
        <input type="text" required value=""  name="email"/>
      </label>
      <label class=" -number">
        <p>Username</p>
        <input type="text" required value=""  name="uname"/>
      </label>
      <label>
        <p>User-Type</p>
        <input type="text" value="" required=""  name="type"/>
      </label>
      <label>
        <p>Works at</p>
        <input value=""  name="work_at"/>
      </label>
      <label>
          <p>Website</p>
        <input value=""  name="website"/>
      </label>
      <label>
        <p>About me</p>
        <textarea name="about_me">
        </textarea>
      </label>
    </div>
    <div>
      <label>
          <p>Facebook</p>
        <input value="" name="facebook" />
      </label>
      <label>
          <p>Qoute</p>
        <input value=""  name="qoute"/>
      </label>
      <label>
          <p>Twitter</p>
        <input value=""  name="twitter"/>
      </label>
      <label>
          <p>LinkedIn</p>
        <input value=""  name="linkedin"/>
      </label>    
      <label>
          <p>Github</p>
        <input value=""  name="github"/>
      </label>
      <label>
          <p>Password(Hashed)</p>
        <input type="password" value="" required  name="password"/>
      </label>
      <label>
          <p>Confirm Password</p>
        <input type="password"/>
      </label>
        <button type="submit">
          Submit
        </button>
    </div>
  </div>
  </form>

@endsection