<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="id" content="">
        <meta name="auth-id" content="guest">
        <title>CodeSnippet - Adminpanel</title>
        <link rel="stylesheet" href="http://codesnippet.local/css/app.css">
        <link rel="stylesheet" href="http://codesnippet.local/css/prism.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css">
    </head>
    <body>
        @include('partials.alerts')
        
        <div id="app" class="wrapper ">
            <header class="admin-header">
                <h1>Code Snippet - Admin Panel</h1>
            </header>
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="login">
                            <div class="card-header">
                                <h2>Login</h2>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{route('cms.loginPost')}}">
                                   @csrf
                                    <div>
                                        <label for="email">E-Mail Address</label> 
                                        <div><input id="email" type="email" name="email"></div>
                                    </div>
                                    <div>
                                        <label for="password">Password</label> 
                                        <div><input id="password" type="password" name="password" required="required"></div>
                                    </div>
                                    <div style="margin: 10px 0 20px 0">
                                        <input type="checkbox" class="form-check-input" id="checkRememberMe" name="remember_me" style="float:left; width:5%">
                                         <label for="checkRememberMe">Remember me</label>
                                        
                                       
                                    </div>
                                    <div>
                                        <div><button type="submit">
                                            Login
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous"></script>
          <script src="{{asset('js/main.js')}}"></script>
          <script src="{{asset('js/app.js')}}"></script>
          <script src="{{asset('js/prism.js')}}"></script>
    </body>
</html>