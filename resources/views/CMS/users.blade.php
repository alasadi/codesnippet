@extends('layout.cmsMaster')
@section('wrapperclass', 'snippets meine-snippets')
@section('title', 'CMS Users')
@section('cmscontent')
	<div style="float:right;">
	  <form action="{{route('cms.createUser')}}" method="post" style="width:100%">        
	    <button type="submit" class="createUser">
	          Create User
	    </button>
	  </form>
	</div>
  	<table>
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Name</th>
	      <th>Email</th>
	      <th>Admin</th>
	      <th>Edit</th>
	    </tr>
	  </thead>
	  <tbody>
      @foreach($users as $user)
		@isset($user->profile->email)
	    <tr>
	      	<td> {{$user->profile->id}}</td>
	      	<td>{{$user->profile->firstname}} {{$user->profile->lastname}} </td>
	      	<td>{{$user->profile->email}} </td>
	      	<td>{{$user->type}}</td>
	      	<td>
	      		<a href="{{route('cms.userdetail', $user->id)}}" title="Edit User">
	      			<i class="fas fa-user-edit"></i> 
	      		</a>
      			<form  method="post" action="{{route('cms.deleteUser', $user->id)}}" onclick="return deleteuser()">
      				@csrf
		      		<button type="submit" title="Delete User">
		      		 	<i class="fas fa-user-times"></i>
		      		</button>
	      		</form>

	      	</td>
	    </tr>
	    @endisset
	      @endforeach

	  </tbody>
	</table>
	<script>
		function deleteuser() {
			if (confirm('do you want to delete this user?')) {
				return true;
			}
			return false;
		}
	</script>
@endsection